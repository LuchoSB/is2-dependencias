/* package whatever; // don't place package name! */

import java.util.*;
import java.lang.*;
import java.io.*;

class A
{
	private int a_data;	
	
	A()	
	{
		a_data = 0;
	}
	
	void setAData(int data)
	{
		a_data = data;
	}
	
	int getAData()
	{
		return a_data;
	}
	
}

class B
{
	private int b_data;	
	
	public A dependencyFromA;
	
	B()
	{
		b_data = 0;
	}
	
	void setBData(int data)
	{
		b_data = data;
	}
	
	int getBData()
	{
		return b_data;
	}
	
}

class C
{
	private int c_data;	
	
	public A dependencyFromA;
	public E dependencyFromE;
	
	C()
	{
		c_data = 0;
	}
	
	void setCData(int data)
	{
		c_data = data;
	}
	
	int getCData()
	{
		return c_data;
	}	
}

class D
{
	private int d_data;	
	
	public C dependencyFromC;
	
	D()
	{
		d_data = 0;
	}
	
	void setDData(int data)
	{
		d_data = data;
	}
	
	int getDData()
	{
		return d_data;
	}
	
}

class E
{
	private int e_data;	
	
	public C dependencyFromC;
	public J dependencyFromJ;
	
	E()
	{
		e_data = 0;
	}
	
	void setEData(int data)
	{
		e_data = data;
	}
	
	int getEData()
	{
		return e_data;
	}
	
}

class F
{
	private int f_data;	
	
	public B dependencyFromB;
	public G dependencyFromG;
	
	F()
	{
		f_data = 0;
	}
	
	void setFData(int data)
	{
		f_data = data;
	}
	
	int getFData()
	{
		return f_data;
	}
	
}

class G
{
	private int g_data;	
	
	public D dependencyFromD;
	
	G()
	{
		g_data = 0;
	}
	
	void setGData(int data)
	{
		g_data = data;
	}
	
	int getGData()
	{
		return g_data;
	}
	
}

class H
{
	private int h_data;	
	
	public F dependencyFromF;
	public I dependencyFromI;
	
	H()
	{
		h_data = 0;
	}
	
	void setHData(int data)
	{
		h_data = data;
	}
	
	int getHData()
	{
		return h_data;
	}
	
}

class I
{
	private int i_data;	
	
	public H dependencyFromH;
	
	I()
	{
		i_data = 0;
	}
	
	void setIData(int data)
	{
		i_data = data;
	}
	
	int getIData()
	{
		return i_data;
	}
	
}

class J
{
	private int j_data;	
	
	public I dependencyFromI;
	
	J()
	{
		j_data = 0;
	}
	
	void setJData(int data)
	{
		j_data = data;
	}
	
	int getJData()
	{
		return j_data;
	}
	
}



/* Name of the class has to be "Main" only if the class is public. */
class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
		// your code goes here
	}
}